import csv
import itertools
import json
import os

################################################################
# SHARED FUNCTIONS
################################################################

# Print a human readable version of the individuals dictionary
def write_people_data(individuals_dict, ind_output_file_prefix):
    
    with open(ind_output_file_prefix + ".txt", "w") as ind_output_text_fie:
        individuals_to_print = 1000000
        for ind, ind_rs_list in itertools.islice(
            individuals_dict.items(), individuals_to_print
        ):
            ind_output_text_fie.write(f"{ind}\n")
            for rs, sub_value in ind_rs_list.items():
                ind_output_text_fie.write(f"\t{rs}: {sub_value}\n")
                
# Check if alleles are valid
def valid_alleles(alleles):
    
    # Check if the snp is valid
    if rs_user.startswith("rs"):
        
        # Check if the individual has two alleles
        if len(alleles) == 2:
                
            # Check if the alleles are valid
            if all(char in nucleotides for char in alleles_user):
                
                return True
    
    return False
                
#############################################
# EXTRACT ANCIENT DATA FROM :PED FILE
#############################################

# Data from plink
ped_file_path = "./AADR_54.1/v54.1_1240K_ancient_europ_patho.ped"
map_file_path = "./AADR_54.1/v54.1_1240K_ancient_europ_patho.map"

# Initialize data structures
ancient_people_dic = {}
rs_list = []

# Mapping from plink data to nucletide bases
nucleotide_dict = {1: "A", 2: "C", 3: "G", 4: "T"}

# Use these variables to name the output files
ancient_people_out_prefix = "ancient_people_snps"
ancient_people_storage_file = ancient_people_out_prefix + ".json"

# If the json file with the data already exists do not execute all this code,
# load the data from the file instead
if os.path.exists(ancient_people_storage_file):
    with open(ancient_people_storage_file, "r") as ap:
        ancient_people_dic = json.load(ap)

# If the json file does not exists
else:
    # Open the .map file containing the rs ids
    with open(map_file_path, "r") as map_file:
        # Create a list from map with rs ids as elements
        for line in map_file:
            parameters = line.strip().split()
            rs_list.append(parameters[1])

        # Open the .ped file containing the data for every individual
        with open(ped_file_path, "r") as ped_file:
            # Every line is an individual
            for sample in ped_file:
                # Split the line into the various fields
                sample_fields = sample.strip().split()

                # Genetic ID
                genetic_id = sample_fields[1]

                # Slice the list from the seventh parameter onwards
                alleles = sample_fields[6:]

                # Group the sliced list two by two
                genotypes = [alleles[i : i + 2] for i in range(0, len(alleles), 2)]

                # Create an empty dictionary
                individual_data = {}

                # For every genotype...
                for index, ind_rs_list in enumerate(genotypes):
                    # ...check if it there is data...
                    if ind_rs_list != ["0", "0"]:
                        # ...convert it to nucleotide bases...
                        converted_value = [
                            nucleotide_dict[int(num)] for num in ind_rs_list
                        ]

                        # ...add to the individual data dictionary, using the rs id as the the key
                        # and the genotype as the value
                        individual_data[rs_list[index]] = converted_value

                # If the individual has a pathogenic marker
                if individual_data:
                    # ...add the individual and all its data to the dictionary with all the individuals,
                    # the key is the individual genetic id
                    ancient_people_dic[genetic_id] = individual_data

    # Save everything to json so it is faster to execute the program again
    with open(ancient_people_storage_file, "w") as output_file:
        json.dump(ancient_people_dic, output_file)

    write_people_data(ancient_people_dic, ancient_people_out_prefix)

###########################################################
# EXTRACT DATA FROM CLINVAR
###########################################################

# Path to the clinvar data
clinvar_data_path = "./ClinVar/variant_summary_pathogenic_snps.txt"
# Dictionary with alleles data from clinvar
rs_alt_allele = {}

# Use these variable to name the output files
rs_alt_allele_prefix = "rs_alt_allele"
rs_alt_allele_storage_file = rs_alt_allele_prefix + ".json"

# If the json file already exists read the needed data from it
if os.path.exists(rs_alt_allele_storage_file):
    with open(rs_alt_allele_storage_file, "r") as cv:
        rs_alt_allele = json.load(cv)

# Otherwise open the clinvar data
else:
    with open(clinvar_data_path, "r") as clinvar_file:
        # Read everything into a dictionary
        clinvar_data = csv.DictReader(clinvar_file, delimiter="\t")

        # Loop over every variant
        for variant in clinvar_data:
            # Retreive data for every variant
            rs_number = variant["RS# (dbSNP)"]
            alternate_allele = variant["AlternateAlleleVCF"]
            marker_name = variant["Name"]
            clinical_significance = variant["ClinicalSignificance"]

            # Save the phenotypes in a list, remove the useless ones
            phenotype_list = []
            unwanted_phenotypes = ["not provided", "not specified"]
            for phenotype in variant["PhenotypeList"].split("|"):
                if phenotype.lower() not in unwanted_phenotypes:
                    phenotype_list.append(phenotype)

            # Save the desired data in a list inside a dictionary that uses the rs number as the key
            rs_alt_allele[f"rs{rs_number}"] = [alternate_allele, phenotype_list]

    # Write the ouput to a json file for quick retrieval
    with open(rs_alt_allele_storage_file, "w") as output_file:
        json.dump(rs_alt_allele, output_file)

    # Write it also in a human readable txt
    with open(rs_alt_allele_prefix + ".txt", "w") as output_file:
        for rs_number, alternate_allele in rs_alt_allele.items():
            output_file.write(f"{rs_number}: {alternate_allele}\n")

##########################################################
# READ USER DATA
##########################################################

# Use these variable to name the output files
users_out_prefix = "users_data"
users_storage_file = users_out_prefix + ".json"

# Dictionary with data from all the users
users_data_dic = {}

# List of user csv files
user_files = {
    "./TestUsers/Test1_DNA.txt": [3],
    "./TestUsers/Test2.csv": [3, 4],
    "TestUsers/Test3.csv": [3],
    "TestUsers/Test4_DNA.txt": [3, 4],
    "TestUsers/Test5_DNA.txt": [3, 4],
}

# Initialize the counter
file_user_counter = 0

# Valid nucleotides
nucleotides = ["A", "C", "G", "T"]

# If the json file with the data already exists do not execute all this code,
# load the data from the file instead
if os.path.exists(users_storage_file):
    with open(users_storage_file, "r") as us:
        users_data_dic = json.load(us)

# If the json file does not exists
else:

    # Loop over each user file
    for user_file_name, alleles_range in user_files.items():
        
        # Get the file extension
        user_file_extension = os.path.splitext(user_file_name)[1]
        
        # Open the user file
        with open(user_file_name, "r") as user_file:
            
            # Discard the header
            next(user_file)

            # Create an empty dictionary
            user_data = {}

            if user_file_extension == ".txt":

                # Loop over each line in the file
                for line in user_file:
                    # Split the line by tabs
                    columns = line.strip().split("\t")

                    # Use the value in the first column as the key
                    rs_user = columns[0]

                    # Create a list of characters from the strings in the columns specified by alleles_range
                    alleles_user = [char for i in alleles_range for char in columns[i]]
                    
                    if valid_alleles(alleles_user):
                        # Add the key-value pair to the dictionary
                        user_data[rs_user] = alleles_user
                    
            if user_file_extension == ".csv":
                
                # Create a csv reader object
                reader = csv.reader(user_file)

                # Loop over each row in the csv file
                for index, row in enumerate(reader):
                    # Get the key from the first column
                    rs_user = row[0]

                    # Get the values from the fourth and fifth column
                    alleles_user = [char for i in alleles_range for char in row[i]]
                
                    if valid_alleles(alleles_user):
                        # Add the key-value pair to the dictionary
                        user_data[rs_user] = alleles_user

            # Increase file counter
            file_user_counter += 1

            # Save user data
            users_data_dic[f"user{file_user_counter}"] = user_data

    # Write the dictionary to the JSON file
    with open(users_storage_file, "w") as users_out_json:
        json.dump(users_data_dic, users_out_json)

    write_people_data(users_data_dic, users_out_prefix)
    
#####################################################
# MAP PEOPLE TO THE DISEASES
#####################################################

def extract_pathogenic_markers(individuals_dic, rs_pathogenic_alleles, ind_path_out_prefix):
    
    ind_pathogenic_rs_storage_file = ind_path_out_prefix + ".json"

    ind_mark_dict = {}

    # Loop over the ancient individuals
    for ind, ind_rs_list in individuals_dic.items():
        # List of markers of every individual
        markers_list = []

        # Loop over every snp of the individual
        for rs in ind_rs_list.keys():
            # Check if the snp is in clinvar
            if rs in rs_pathogenic_alleles:
                # Read the two alleles of the individual
                ind_allele_1 = individuals_dic[ind][rs][0]
                ind_allele_2 = individuals_dic[ind][rs][1]

                # Read the mutation that trigger the disease
                dis_allele = rs_pathogenic_alleles[rs][0]

                # Check if the individual has the mutation
                if ind_allele_1 == dis_allele or ind_allele_2 == dis_allele:
                    # If the individual has the mutation, append it to the list
                    markers_list.append(rs)

        # If the individual has some pathogenic markers...
        if markers_list:
            # Append the individual data to the final dictionary
            ind_mark_dict[ind] = markers_list

    # Write the ouput to a json file for quick retrieval
    with open(ind_pathogenic_rs_storage_file, "w") as ind_pathogenic_rs_json:
        json.dump(ind_mark_dict, ind_pathogenic_rs_json)

    with open(f"{ind_path_out_prefix}.txt", "w") as ind_pathogenic_rs_txt:
        for ind, ind_rs_list in ind_mark_dict.items():
            ind_pathogenic_rs_txt.write(f"{ind}: {ind_rs_list}\n")
            
    return ind_mark_dict

ancient_people_path_rs_basename = "ancient_people_path_rs"
users_path_rs_basename = "users_path_rs"

if os.path.exists(f'{ancient_people_path_rs_basename}.json') and os.path.exists(f'{users_path_rs_basename}.json'):
    with open(f'{ancient_people_path_rs_basename}.json', 'r') as ancient_people_path_rs_json, open(f'{users_path_rs_basename}.json', 'r') as users_path_rs_json:
        ancient_people_path_rs_dic = json.load(ancient_people_path_rs_json)
        users_path_rs_dic = json.load(users_path_rs_json)
else:
    ancient_people_path_rs_dic = extract_pathogenic_markers(ancient_people_dic, rs_alt_allele, ancient_people_path_rs_basename)
    users_path_rs_dic = extract_pathogenic_markers(users_data_dic, rs_alt_allele, users_path_rs_basename)


###############################################################
# COMPARE USERS TO ANCIENT PEOPLE
###############################################################

# Use these variable to name the output files
matching_markers_out_prefix = "matching_markers"
matching_markers_storage_file = matching_markers_out_prefix + ".json"

# If the json file with the data already exists do not execute all this code,
# load the data from the file instead
if os.path.exists(matching_markers_storage_file):
    with open(matching_markers_storage_file, "r") as mm:
        matching_individuals = json.load(mm)

# If the json file does not exists
else:

    # Dictionary with the matching individuals
    matching_individuals = {}

    for user_key, user_markers in users_path_rs_dic.items():
        max_shared_markers = 0
        matching_ancient_person = None
        shared_markers = []
        
        for ancient_key, ancient_markers in ancient_people_path_rs_dic.items():
            current_shared_markers = list(set(user_markers) & set(ancient_markers))
            current_shared_markers.sort()
            
            if len(current_shared_markers) > max_shared_markers:
                max_shared_markers = len(current_shared_markers)
                matching_ancient_person = ancient_key
                shared_markers = current_shared_markers
        
        if matching_ancient_person is not None:
            matching_individuals[user_key] = {"matching_ancient_person": matching_ancient_person, "shared_markers": shared_markers}

    # Write the dictionary to the JSON file
    with open(matching_markers_storage_file, "w") as matching_markers_out_json:
        json.dump(matching_individuals, matching_markers_out_json)

    # Write the dictionary to the TXT file
    with open(f'{matching_markers_out_prefix}.txt', "w") as matching_markers_out_txt:
        for user_key, user_data in matching_individuals.items():
            matching_ancient_person = user_data["matching_ancient_person"]
            shared_markers = user_data["shared_markers"]
            matching_markers_out_txt.write(f"User: {user_key}\n")
            matching_markers_out_txt.write(f"Matching Ancient Person: {matching_ancient_person}\n")
            matching_markers_out_txt.write("Shared Markers:\n")
            for marker in shared_markers:
                matching_markers_out_txt.write(f"- {marker}\n")
            matching_markers_out_txt.write("\n")