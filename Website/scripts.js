document.addEventListener("DOMContentLoaded", function () {

    // Load all the necessary files asynchronously
    Promise.all([
        fetch("./users_path_rs.json").then(result => result.json()),
        fetch("./ancient_people_path_rs.json").then(result => result.json()),
        fetch("rs_alt_allele.json").then(result => result.json()),
        fetch('./matching_markers.json').then(response => response.json())
    ]).then(([users, ancientPeople, pathogenicSnps, matchingMarkers]) => {

        // Fill in users table
        for (const individual in users) {
            appendToIndividualsTable(individual, users[individual], "table_users", pathogenicSnps, true);
        }

        // fill in ancient people table
        for (const individual in ancientPeople) {
            appendToIndividualsTable(individual, ancientPeople[individual], "table_ancient", pathogenicSnps, false);
        }

        // Fill in users vs ancient people table
        appendToUserVsAncientTable(matchingMarkers, "table_users_vs_ancient");
    });
});

// This function is used to append an individuals to either the users or ancient people table
function appendToIndividualsTable(individual_name, individual_data, table_id, pathogenic_snps, rs_description_tooltip_enabled) {

    // Create a new row for the new individual
    table_ind = document.getElementById(table_id);
    const values = individual_data;
    const row = table_ind.insertRow();
    const keyCell = row.insertCell();
    keyCell.textContent = individual_name;

    // Create a number of cells in the right column and fill them with the pathogenic snps of the individual
    keyCell.setAttribute('rowspan', values.length + 1);
    for (let i = 0; i < values.length; i++) {
        const newRow = table_ind.insertRow();
        const valueCell = newRow.insertCell();
        const tooltip = document.createElement('div');
        tooltip.textContent = values[i];
        valueCell.appendChild(tooltip);

        // Time consuming! There is a flag to enable/disable the function
        // For every snp, search the associated diseases and write them to the tooltip
        if (rs_description_tooltip_enabled) {
            tooltip.classList.add('tooltip');
            for (const snpsKey in pathogenic_snps) {
                if (snpsKey === values[i]) {
                    const tooltiptext = document.createElement('span');
                    tooltiptext.classList.add('tooltiptext');
                    tooltiptext.textContent = pathogenic_snps[snpsKey][1].join('\r\n');
                    tooltip.appendChild(tooltiptext);
                    break;
                }
            }
        }
    }
}

// Append an array of users to the users vs ancient people table
function appendToUserVsAncientTable(people_data, table_id) {

    table_users_vs_ancient = document.getElementById(table_id);

    // For every user create a new table...
    for (const person_name in people_data) {
        const row = table_users_vs_ancient.insertRow();
        const keyCell = row.insertCell();
        
        // ...write the user name in the first column
        keyCell.textContent = person_name;
        const valueCell = row.insertCell();

        // ...write the matching ancient person in the second column
        valueCell.textContent = people_data[person_name]['matching_ancient_person'];
        const markers = people_data[person_name]['shared_markers'];
        keyCell.setAttribute('rowspan', markers.length + 1);
        valueCell.setAttribute('rowspan', markers.length + 1);

        // ... and write the list of shared markers in the third column
        for (const marker of markers) {
            const newRow = table_users_vs_ancient.insertRow();
            const markerCell = newRow.insertCell();
            markerCell.textContent = marker;
        }
    }
}

// Called when the user select a table from the dropdown menu
// Set the visibility of the tables according to the user choice
function changeTable(tableId) {
    if (tableId === 'table_users') {
        table_users.style.display = 'block';
        table_ancient.style.display = 'none';
        table_users_vs_ancient.style.display = 'none';
    } else if (tableId === 'table_ancient') {
        table_users.style.display = 'none';
        table_ancient.style.display = 'block';
        table_users_vs_ancient.style.display = 'none';
    } else if (tableId === 'table_users_vs_ancient') {
        table_users.style.display = 'none';
        table_ancient.style.display = 'none';
        table_users_vs_ancient.style.display = 'block';
    }
}

// Valid nucleotides
const nucleotides = ["A", "C", "G", "T"];

// Alleles range in csv files
const allelesRange = [3, 4];

// Function to check if alleles are valid
function validAlleles(alleles) {
    return alleles.every(allele => nucleotides.includes(allele));
}

// Function to handle file upload by the user
function handleFileUpload() {

    // Read the data from the form
    const fileInput = document.getElementById('fileInput');
    const file = fileInput.files[0];
    const nameInput = document.getElementById('nameInput');
    const userDataName = nameInput.value;

    // Save file data to meaningful variables
    const fileName = file.name;
    const fileNameWithoutExtension = fileName.split('.').slice(0, -1).join('.');
    const fileSize = file.size;
    const fileType = file.type;

    // Log to console, it does not hurt and helps with debugging
    console.log("User Name:", userDataName);
    console.log("File Name:", fileNameWithoutExtension);
    console.log("File Size:", fileSize);
    console.log("File Type:", fileType);

    // Read the content of the file uploaded by the user
    const reader = new FileReader();
    reader.onload = function (event) {
        const fileContent = event.target.result;

        // Discard the ending new line, the first line and remove '\r'
        const lines = fileContent.trim().split('\n').slice(1).map(line => line.replace('\r', ''));
        const markersList = [];

        // Fetch the rs_alt_allele.json file once
        // Needed to check for pathogenic snps
        fetch('rs_alt_allele.json')
            .then(response => response.json())
            .then(rsPathogenicAlleles => {
                const pathogenicMarkers = new Set(Object.keys(rsPathogenicAlleles));

                // Loop line by line
                for (const line of lines) {
                    const columns = line.split(',');

                    // Get the rs id from the first column
                    const rsUser = columns[0];

                    // Get the alleles from specified columns
                    const allelesUser = columns[3].split('');

                    // Check if the alleles are valid and the snp might be pathogenic
                    if (validAlleles(allelesUser) && pathogenicMarkers.has(rsUser)) {

                        // Read the two alleles of the individual
                        const [indAllele1, indAllele2] = allelesUser;

                        // Read the mutation that triggers the disease
                        const disAllele = rsPathogenicAlleles[rsUser][0];

                        // Check if the individual has the mutation
                        if (indAllele1 === disAllele || indAllele2 === disAllele) {

                            // If the individual has the mutation, append it to the list
                            markersList.push(rsUser);
                        }
                    }
                }

                // If the individual has some pathogenic markers...
                if (markersList.length > 0) {
                    // Append the individual data to the final dictionary
                    console.log("User Marker:", markersList);
                    userMarkers = markersList;
                    appendToIndividualsTable(userDataName, markersList, "table_users", true);

                    // Load the data of the ancient people
                    fetch("./ancient_people_path_rs.json")
                        .then(result => result.json())
                        .then(ancientPeopleSnps => {

                            // Set the shared pathogenic snps to zero
                            let maxSharedSnps = 0;

                            // Create an object in which to save the match info
                            let userMatchingSnps = {
                                [userDataName]: {
                                    matching_ancient_person: null,
                                    shared_markers: []
                                }
                            };

                            // Loop over the ancient people to find the one that shares the most snps
                            for (const ancientPerson in ancientPeopleSnps) {
                                const ancientPersonSnps = ancientPeopleSnps[ancientPerson];
                                const sharedSnps = ancientPersonSnps.filter(snp => markersList.includes(snp));

                                // If an ancient person share more snps with the user than the current one update the pointer
                                if (sharedSnps.length > maxSharedSnps) {
                                    maxSharedSnps = sharedSnps.length;
                                    userMatchingSnps[userDataName].matching_ancient_person = ancientPerson;
                                    userMatchingSnps[userDataName].shared_markers = sharedSnps;
                                }
                            }

                            // If there is a match add it to the table
                            if (userMatchingSnps[userDataName].matching_ancient_person) {
                                console.log("Ancient Person with Max Shared SNPs:", userMatchingSnps[userDataName].matching_ancient_person);
                                console.log("Shared Markers:", userMatchingSnps[userDataName].shared_markers);
                                appendToUserVsAncientTable(userMatchingSnps, "table_users_vs_ancient");
                            } else {
                                console.log("No ancient person found with shared SNPs");
                            }
                        });

                }
                else {
                    console.log("No pathogenic markers found");
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
    };

    reader.readAsText(file);
}
