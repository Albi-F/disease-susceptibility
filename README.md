# Check Me

## The project

The goal of Check Me is to offer an easy interface to quickly get an overview of the disease susceptibility of a user. In addition, it allows the user to find out to which ancient person his/her susceptibility profile is mostly similar. 

## How to run the program

To run this program it is necessary to install [VS Code](https://code.visualstudio.com/) and the extension (Live Preview from Microsoft)[https://marketplace.visualstudio.com/items?itemName=ms-vscode.live-server]. Once both are installed open the `index.html` file in vs code, press ctrl + shift + P to open the command palette and execute the command `Live Preview: Show Preview (External Browser)`. Alternatively, you can use the online version available (here)[https://albi-f.gitlab.io/disease-susceptibility/].

## Known bugs

- It is possible to submit a `.csv` file even if the name of the user is not specified.
- It is possible to have two users with the same name.

## Generate the .json files for the website from the raw data

The file `Project.ipynb` in the Analysis folder is a jupiter notebook with the code necessary to filter the plink data that given its size can not be operated on with Python. The functions must be executed in the order they are presented and requires conda to install some python modules and other programs.

After the execution of the jupyter notebook and the creation of the `.ped` and `.map` files with plink and of the filtered version of the ClinVar data (with only the pathogenic SNPs), it is necessary to execute the python script called `data_parser.py` situated in the same folder. This script has hardcoded paths that can be easily changed if necessary and takes in input the data from plink, ClinVar and the test users and generate a series of json files:

- The following are intermediary files not used by the website:

  - `users_data.json` -> this file contains the genotype of test users
  - `ancient_people_snps.json` -> genotype of the ancient people

- These files are the ones necessary for the website:

  - `rs_alt_allele.json` -> for every pathogenic snp it contains the allele associate and the disease associated with it
  - `users_path_rs.json` -> contains the list of pathogenic alleles of the test users
  - `ancient_people_path_rs.json` -> contains the list of pathogenic alleles of the ancient people
  - `matching_markers` -> contains the matches between users and ancient people

These data is loaded from json files instead of being calculated at every start-up to save processing time when opening the website, it is nevertheless possible to add new users from the web interface.

## Test files

The code has been tested with the provided test users, furthermore a new test user have been created by extracting a subset from `Test3.csv` in order to test the upload on the website. Because of the speed of JavaScript the website takes quite a long time to analyze files with tens of thousands of snps.